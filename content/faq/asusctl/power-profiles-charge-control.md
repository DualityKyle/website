+++
title = "I don't have any power profiles or charge control"
+++

Ensure you are using kernel 5.11 at minimum. We recommend to use at least 5.13 so that you get all the most recent patches and fixes for ASUS laptops.

It's also possible that your laptop doesn't support this so if the kernel update doesn't solve this feel free to make a :sadface: (sorry).