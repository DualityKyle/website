+++
title = "New kernel patches, new generation"
date = 2023-06-30

[extra]
author = "Luke Jones"
+++

It has been quite a while since the last update from me (fluke).

As seems to be typical so far, ASUS has released a new generation of machines with a new generation
of issues. Most of the time these can be very easy to fix - things like new keyboard identifiers,
or key scancodes, or even new WMI methods (in ACPI).

# Patches

## HID driver patches:

**[LKML patch series](https://lkml.org/lkml/2023/5/2/1006)**

This patch series adds the Z13 laptop keyboard identifier, some new keys, and does a small tidy up of
some code.

## Sound quirks:

**[LKML patch series](https://lkml.org/lkml/2023/5/2/1011)**

This adds another quirk for the GU603 (2022 M16) Realtek codec. Looks like there can be more than one
version of the codec in a model range. Fortunately this was a quick fix since nothing else appears to
have changed.

## WMI - Screenpad support

**[V1 LKML patch series](https://lkml.org/lkml/2023/5/3/1)**
**[V2 LKML patch series](https://lkml.org/lkml/2023/5/5/9)**
**[V3 LKML patch series](https://lkml.org/lkml/2023/6/19/1277)**

The initial patch here was to add two new sysfs platform entries
- `/sys/devices/platform/asus-nb-wmi/screenpad_brightness`
- `/sys/devices/platform/asus-nb-wmi/screenpad_enable`

but as mentioned in a reply to the patch, adding as a proper backlight would be better.

So I spent a day of time rewriting the entire thing, and with the help of a very kind tester I think the resulting patch is perfect.
There is now a new backlight device under `/sys/class/backlight/asus_screenpad` which can be controlled using all standard methods.

**Update**: I need to submit a v4 as I made a mistake where if the screenpad power is off the dev struct is dereferenced before being initialised (thanks C).

# Feature enablement

**[LKML patch series](https://lkml.org/lkml/2023/6/19/1264)**

This patch series adds or exposes more features that are available in the ROG
laptop series.

- expose dGPU and CPU tunables for ROG
  - These are things like GPU boost, CPU Pl1 and PL2, package power limits
- support setting mini-LED mode
  - Some newer laptops have a screen that can toggle between regular style backlight and using mini-LED backlight
- add WMI method to show if egpu connected
  - This WMI method can be monitored/queried to see if it is possible to begin the change-over to eGPU
- support middle fan custom curves
  - Some newer laptops have a center/middle fan which blows across the CPU and GPU
- add support for showing middle fan RPM
- add support for showing charger mode (AC, USB-C, both plugged)
- add additional checks to GPU switching code
  - These try to prevent a sceanrio such as the user disabling the dGPU while it
    is driving the internal panel via MUX, resulting in no output at all.
    There are no checks in the ACPI code for this, but on some newer models ASUS
    did finally add a switch in the BIOS menu. It is best to try and prevent this
    at the kernel level rather than userland level.

I need to update one patch in this series to correctly name the dGPU and CPU tunables.

# Sound

This year sound is... consistent across almost all the range. So far what is required is:

1. An ssdt patch to add the missing _DSD (device specific data) containing the Cirrus amplifier properties
2. A kernel patch to add the required quirk to enable either the I2C connected amp, or an SPI connected amp

I will be writing a short guide to add to the website later to help people enable sound if required. But this may not be a very convenient thing as I am trying to work with both ASUS and Cirrus folks to get this corrected in a way that is safe. the ideal solution would be to add the missing properties to the ACPI, but this is unlikely to happen, and it's not just ASUS doing this either - I'm unsure what the exact solution will be but it is certainly being worked on.
