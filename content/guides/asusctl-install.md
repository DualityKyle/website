+++
title = "General Distro Install"
description = "General steps to install asusctl and supergfxctl on most distro"
date = 2021-07-15
updated = 2023-07-22

[extra]
author = "Luke Jones"
+++

This is a very general and basic guide for general Linux distrobutions.

Distros that we have full guide and official package supported:

  - Fedora Workstation
  - Arch Linux
  - OpenSUSE

You can find all the guides in our `Guides` page.

Distros that very popular but we don't have offcial supported:

  - Debian and Debian based (such as Ubuntu/PopOS)
  - Manjaro
  - CentOS/RockyOS or any similar

But why?

1. Old kernel: many patches that drastically improve Linux experience on an ASUS/ROG laptop are only available in the latest kernel. The minimum kernel version we recommend now is >= 6.1 (newer is better), which is why you should never run CentOS/RockyOS on newer devices, especially a laptop.
2. Too many custom changes: such as PopOS and Manjaro, all the custom kernel/package stuff will very likely conflict with asusctl and supergfxctl and will not be functional.

However, if you **REALLY REALLY** need that very specific distro to get your job done, we strongly recommend using [DistroBox](https://github.com/89luca89/distrobox) to provide the environment that the software needs. You can find many youtube videos show you how to use it.

Anyway you can still building asusctl and supergfxctl from source code, Please see the [asusctl repository](https://gitlab.com/asus-linux/asusctl) and [supergfxctl repository](https://gitlab.com/asus-linux/supergfxctl) for source code and guidance.

Before starting your adventure, make sure:
- it is systemd base
- must Linux, not BSD or so
- update your distro
- install GPU drivers
- remove any distro provided methods of graphics switching (like prime)
- reboot

After finishing the installation. Reboot and congratulations, everything should running now. Using `asusctl --help` and `supergfxctl --help` will provide some instructions on how/what to set up via CLI.
