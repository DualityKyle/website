+++
title = "asusctl-tray"
weight = 3
+++

A simple tray application to switch between graphics modes via supergfxctl. This is for this who can't use the GNOME Shell extension [supergfxctl-gex](https://extensions.gnome.org/extension/5344/supergfxctl-gex/), or the KDE plasmoid [supergfxctl-plasmoid](https://gitlab.com/Jhyub/supergfxctl-plasmoid) - or simply just want to use something more lightweight.

![asusctl-tray screenshot](/images/third-party/asusctl-tray.jpg)

The source code can be found here - [https://github.com/Baldomo/asusctltray](https://github.com/Baldomo/asusctltray)