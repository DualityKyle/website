+++
title = "Switching graphics modes (integrated/hybrid/dedicated) doesn't seem to work"
+++

There's a few steps to troubleshoot.

1. If you are dual-booting Windows 10 / 11 (or think Windows may have been in iGPU Mode in Armoury Crate) then try this,

`journalctl -b -u supergfxd`

If there are lines which end with `ERROR: Could not find dGPU`, then reboot into Windows and make sure that Armoury Crate has the dGPU set to Auto Mode.  If you have set it to iGPU only mode, then the dGPU will be forcibly removed from the system and Linux will no longer be able to see it.

If you no longer have Windows installed, and you suspect that it was set to iGPU only mode when you installed Linux, then you can try to forcibly enable the dGPU with the following.

```
echo 0 | sudo tee /sys/devices/platform/asus-nb-wmi/dgpu_disable
echo 1 > sudo /sys/bus/pci/rescan
echo 0 | sudo tee /sys/devices/platform/asus-nb-wmi/dgpu_disable
```

It may take a few tries to enable it, and a reboot is recommended afterwards.

2. Are your Nvidia drivers installed correctly?

In hybrid or nvidia mode they should be loaded, check with `lsmod |grep nvidia` for similar to:
```
[luke@datatron]$ lsmod |grep nvidia
nvidia_drm             65536  11
nvidia_modeset       1232896  18 nvidia_drm
nvidia_uvm           1150976  0
nvidia              34185216  906 nvidia_uvm,nvidia_modeset
drm_kms_helper        274432  2 nvidia_drm,i915
drm                   618496  14 drm_kms_helper,nvidia_drm,i915
```
if that doesn't show similar, then you need to reinstal your Nvidia drivers.

3. Is Nouveau blocklisted correctly? It's common to require the following in `/etc/default/grub`:
```
GRUB_CMDLINE_LINUX="rd.driver.blacklist=nouveau modprobe.blacklist=nouveau"
```
that line may not be exact, and *do not* delete existing entry to match it, just add those two items to the end of the line before the last "

4. What does `journalctl -b -u asusd` say after a failed switch?

If it contains lines like the following then we need to look at active sessions
```
asusd[1373]: INFO: GFX: Switching gfx mode to integrated
asusd[1373]: INFO: GFX: display-manager thread started
asusd[1373]: WARN: GFX: thread time exceeded 3 minutes, exiting
```
Do: `loginctl list-sessions`, if it shows more than one session lets have a look with:
```
loginctl show-session --property Type --property Class --property Active <num>
```
where `<num>` is the number or char+num from the `SESSION` column of the output above.
If there is more than one `active` session of type `Wayland | X11 | Mir` then somehow
you've ended up with an extra session.

Save your work, `Ctrl+Alt+F4` (or any F`<num>`) key to a TTY terminal session and run
```
asusctl graphics -m <desired mode>
sudo loginctl kill-user <username>
```
your display-manager should then restart.

5. If none of the above helps, and/or you use KDE, and/or have a G14 with GTX
graphics, you may need to this,
```
sudo sed -i 's/#KillUserProcesses=no/KillUserProcesses=yes/' /etc/systemd/logind.conf
```
then `sudo systemctl restart display-manager`.