+++
title = "How do I set a custom fan curve?"
+++

Custom fan curves (not speaking of the built in power profiles) are currently only supported on Ryzen ROG laptops.<br>
The necessary kernel patches are merged since 5.17.

The format is shown [here](https://github.com/cronosun/atrofac/blob/master/ADVANCED.md#limits).

There are three fan profiles namely Quiet, Balanced and Performance to choose from. Each profile is linked to power profile and gets applied when the power profile is set. You can enable/disable the fan profiles using the following command:
```
asusctl fan-curve -m <profile_name> -e true/false
```
All three fan profiles can be activated at once. If no profile is activated manually then the fan curve from the BIOS is used.

To change the fan curve data for a specific profile use the following command:
```
asusctl fan-curve -m <profile_name> -D <fan_curve_data>
```