+++
title = "Pressing Fn+F5 doesn't do anything"
+++

You need to map the key-combo to an action in your desktop, like this:

![](/images/faq/fan-shortcut.png)