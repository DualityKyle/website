+++
title = "It's not working!"
+++

Check the logs with `sudo journalctl -b -u asusd` and look for errors.