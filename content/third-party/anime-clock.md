+++
title = "AniMe Matrix - Clock"
weight = 5
+++

A simple python scrip that displays the current time on your AniMe Matrix display! Requires `asusctl`, `python3`, and `pip` to be installed.

![AniMe matrix clock screenshot](/images/third-party/anime-clock.jpg)

You can download the script and view the source code here - [https://github.com/Blobadoodle/anime-matrix-clock](https://github.com/Blobadoodle/anime-matrix-clock)